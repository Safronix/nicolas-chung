start:
	@cd ./api && docker compose down && docker compose up -d --build
	@cd ./front && npm install && npm run stop && (npm run start&)
	@sleep 30
	@$(MAKE) safronix_framework_migrate
	@$(MAKE) migrate
	@$(MAKE) save_static
	@$(MAKE) db_entries
	@$(MAKE) super_user
	@$(MAKE) messages

diff:
	@cd ./api && docker compose exec php bin/console doc:mig:diff

safronix_framework_migrate:
	-@cd ./api && docker compose exec php bin/console doc:mig:execute --up 'App\SafronixFramework\Migrations\Version20220610082151' -n
	-@cd ./api && docker compose exec php bin/console doc:mig:execute --up 'App\SafronixFramework\Migrations\Version20230517070622' -n
	-@cd ./api && docker compose exec php bin/console doc:mig:execute --up 'App\SafronixFramework\Migrations\Version20230621093428' -n
	-@cd ./api && docker compose exec php bin/console doc:mig:execute --up 'App\SafronixFramework\Migrations\Version20230703084008' -n
	-@cd ./api && docker compose exec php bin/console doc:mig:execute --up 'App\SafronixFramework\Migrations\Version20230703093913' -n
	-@cd ./api && docker compose exec php bin/console doc:mig:execute --up 'App\SafronixFramework\Migrations\Version20230708100025' -n

migrate:
	@cd ./api && docker compose exec php bin/console doc:mig:mig -n

db_entries:
	@cd ./api && docker compose exec php bin/console app:create-default-accounts
	@cd ./api && docker compose exec php bin/console app:create-default-crypto-currencies

super_user:
	@cd ./api && docker compose exec php bin/console app:set-super-users

workers:
	@cd ./api && docker compose exec php bin/console messenger:consume async --time-limit=60 --limit=100 -vv

save_static:
	@cd ./api && docker compose exec php bin/console app:save-geonames
	@cd ./api && docker compose exec php bin/console app:save-crypto-currencies

mysql_connect:
	@cd ./api && docker compose exec mysql /bin/bash -c 'mysql -u$$MYSQL_USER -p$$MYSQL_PASSWORD'

messages:
	@cd ./api && docker compose exec php bin/console app:messages
	@$(MAKE) workers

test:
	@cd ./api && docker compose exec php bin/console app:test
